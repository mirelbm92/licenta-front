(function(){
    'use strict';

    /**
     * This is a sample controller
     * Generated by ng-appgen Yeomen/Angular generator.
     *
     * @author: Tapas Jena
     * @copyright: Anitech Consulting Services Pvt Ltd.
     */
    angular.module('home').controller('homeController', function($scope, homeService, $localStorage) {

        // get message from service
        $scope.message = homeService.getMessage();
        // $scope.mySlides = [
        //     {
        //         url: "assets/imgs/logo_site.png",
        //         state: 'home'
        //     },
        //     {
        //         url: 'http://www.dogbreedslist.info/uploads/allimg/dog-pictures/Pomeranian-1.jpg',
        //         state: 'help'
        //     }
        // ];
        

    });

})();
