(function(){
    'use strict';

    /**
     * This is a sample controller for application level common tasks
     * Generated by ng-appgen Yeomen/Angular generator.
     *
     * @author: Tapas Jena
     * @copyright: Anitech Consulting Services Pvt Ltd.
     */
    angular.module('common').controller('commonController', function($scope, $state, $localStorage, Notification) {

        //TODO: Implement your controller logic here
        $scope.stateName = $state.current.name;
        $scope.name = $localStorage.user.name;
        $scope.userdata = $localStorage.user;
        $scope.logout = function() {
            Notification.success('Userul a fost delogat');
            $localStorage.$reset();
            $state.go('login');

        };

    });

})();
